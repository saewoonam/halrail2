/***************************************************************************//**
 * @file main.c
 * @brief Simple RAIL application which includes hal
 * @copyright Copyright 2017 Silicon Laboratories, Inc. http://www.silabs.com
 ******************************************************************************/
#include "rail.h"
#include "hal_common.h"
#include "rail_config.h"
#include "bsp.h"
#include "retargetserial.h"
#include "gpiointerrupt.h"
#include <stdio.h>
#include "em_chip.h"

#if defined(HAL_CONFIG)
#include "hal-config.h"
#endif

#define PACKET_LENGTH 32

/* 536872172, */

typedef struct ButtonArray{
  GPIO_Port_TypeDef   port;
  unsigned int        pin;
} ButtonArray_t;

static const ButtonArray_t buttonArray[BSP_BUTTON_COUNT] = BSP_BUTTON_INIT;
volatile uint16_t packetToReceive[PACKET_LENGTH] = {0,};


static const RAIL_DataConfig_t railDataConfig = {
  .txSource = TX_PACKET_DATA,
  .rxSource = RX_IQDATA_FILTLSB, //if rx_packet_data, works fine, alt is RX_IQDATA_FILTLSB
  .txMethod = FIFO_MODE, //doesn't do anything
  .rxMethod = FIFO_MODE,
};

void gpioCallback(uint8_t pin);

void peripheralInit(){
  RETARGET_SerialCrLf(1);
  for ( int i= 0; i < BSP_BUTTON_COUNT; i++){
    GPIO_PinModeSet(buttonArray[i].port, buttonArray[i].pin, gpioModeInputPull, 1);
  }
  GPIOINT_Init();
  GPIOINT_CallbackRegister(buttonArray[0].pin, gpioCallback);
  GPIOINT_CallbackRegister(buttonArray[1].pin, gpioCallback);
  GPIO_IntConfig(buttonArray[0].port, buttonArray[0].pin, false, true, true);
  GPIO_IntConfig(buttonArray[1].port, buttonArray[1].pin, false, true, true);

}

// Prototypes
void RAILCb_Generic(RAIL_Handle_t railHandle, RAIL_Events_t events);

RAIL_Handle_t railHandle;

static RAIL_Config_t railCfg = {
  .eventsCallback = &RAILCb_Generic,
};

void initRadio()
{
  halInit();
  railHandle = RAIL_Init(&railCfg, NULL);
  if (railHandle == NULL) {
    while (1) ;
  }
  RAIL_ConfigCal(railHandle, RAIL_CAL_ALL);

  // Set us to a valid channel for this config and force an update in the main
  // loop to restart whatever action was going on
  RAIL_ConfigChannels(railHandle, channelConfigs[0], NULL);

  // Initialize the PA now that the HFXO is up and the timing is correct
  RAIL_TxPowerConfig_t txPowerConfig = {
#if HAL_PA_2P4_LOWPOWER
    .mode = RAIL_TX_POWER_MODE_2P4_LP,
#else
    .mode = RAIL_TX_POWER_MODE_2P4_HP,
#endif
    .voltage = BSP_PA_VOLTAGE,
    .rampTime = HAL_PA_RAMP,
  };
  if (RAIL_ConfigTxPower(railHandle, &txPowerConfig) != RAIL_STATUS_NO_ERROR) {
    // Error: The PA could not be initialized due to an improper configuration.
    // Please ensure your configuration is valid for the selected part.
    while (1) ;
  }
  RAIL_SetTxPower(railHandle, HAL_PA_POWER);

  RAIL_StateTransitions_t transitions = {RAIL_RF_STATE_RX, RAIL_RF_STATE_RX};
  RAIL_SetRxTransitions(railHandle, &transitions);
  RAIL_SetTxTransitions(railHandle, &transitions);

  RAIL_ConfigEvents(railHandle, RAIL_EVENTS_ALL, RAIL_EVENTS_TX_COMPLETION | RAIL_EVENTS_RX_COMPLETION | RAIL_EVENT_CAL_NEEDED | RAIL_EVENT_RX_FIFO_ALMOST_FULL);

  RAIL_ConfigData(railHandle, &railDataConfig);
}

#define PAYLOAD_LENGTH 16
#define BUFFER_LENGTH 16

static uint8_t payload[BUFFER_LENGTH] = {PAYLOAD_LENGTH-1, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
volatile RAIL_RxPacketHandle_t packetHandle = RAIL_RX_PACKET_HANDLE_INVALID;
volatile bool bufferfull = false;
volatile bool startTx = false;
volatile uint16_t rxBytes = 0;


void gpioCallback(uint8_t pin){
  startTx = true;
  printf("Button Hit Now \r\n");
}

int main(void)
{
  CHIP_Init();
  initRadio();
  peripheralInit();
  RAIL_StartRx(railHandle, 0, NULL);
  RAIL_SetRxFifoThreshold(railHandle, 256);

  while (1){
    if ( startTx ){
      RAIL_SetTxFifo(railHandle, payload, PAYLOAD_LENGTH, BUFFER_LENGTH);
      if ( RAIL_StartTx(railHandle, 0, RAIL_TX_OPTIONS_DEFAULT, NULL) == RAIL_STATUS_NO_ERROR ){
        startTx = false;
        //sent pulse
      }
    }

    if ( bufferfull ){
    	for(int i = 0; i < 15; i++) {
		printf("%d\n", (int16_t)packetToReceive[i]);
    	}
    	bufferfull = false;
    }

    //if container, should put print from callback here
    //check i^2 and q^2 mag

  }
  return 0;
}

void RAILCb_Generic(RAIL_Handle_t railHandle, RAIL_Events_t events)
{
/*
  if( events & RAIL_EVENT_CAL_NEEDED ){
    RAIL_Calibrate(railHandle, NULL, RAIL_CAL_ALL_PENDING);
  }
*/
  if ( events & RAIL_EVENTS_TX_COMPLETION ){
    BSP_LedToggle(0);
  }
  if ( events & RAIL_EVENTS_RX_COMPLETION ){
    //BSP_LedToggle(1);
    if ( (events & RAIL_EVENT_RX_PACKET_RECEIVED) &&  packetHandle == RAIL_RX_PACKET_HANDLE_INVALID ){
      packetHandle = RAIL_HoldRxPacket(railHandle);
    }
  }

  if (events & RAIL_EVENT_RX_FIFO_ALMOST_FULL ){
	  BSP_LedToggle(1);
	  rxBytes = RAIL_ReadRxFifo(railHandle, packetToReceive, 16);
	  RAIL_ResetFifo(railHandle, false, true);
	  bufferfull = true;
  }
}
